import arcade
from config import WIDTH, SCALING

class Player(arcade.Sprite):
    """
    Player class.
    """

    def __init__(self):
        super().__init__('assets/player.png', SCALING * 1.5)
        self.center_x = WIDTH/2
        self.center_y = 50    