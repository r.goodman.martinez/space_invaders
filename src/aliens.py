import arcade
from config import ALIEN_ANIMATION_TIMING, SCALING, ALIEN_STEP, WIDTH, HEIGHT
import math

class Aliens():
    alien_matrix = None
    alien_bullets = None
    direction = 'RIGHT'
    step_down = 0

    def __init__(self):
        """
        Alien setup function.
        Create a 4x8 matrix with alien animated sprites
        """

        self.alien_matrix = []
        self.alien_bullets = []
        alien_list = arcade.SpriteList()
        #alien_bullets = arcade.SpriteList()
        for i in range(4):
            for j in range(8):
                alien = arcade.AnimatedTimeSprite()
                alien.texture_change_frames = ALIEN_ANIMATION_TIMING
                alien.center_x = (j+1) * 170 * SCALING
                alien.center_y = HEIGHT - ((i+1) * 100 * SCALING)
                alien.textures = []
                alien.textures.append(arcade.load_texture('assets/alien' + str(math.ceil((i + 1)/2)) + '_1.png'))
                alien.textures.append(arcade.load_texture('assets/alien' + str(math.ceil((i + 1)/2)) + '_2.png'))
                alien.scale = SCALING
                alien_list.append(alien)
            self.alien_matrix.append(alien_list)

    def move_aliens(self):
        max_x = 0
        min_x = WIDTH

        #Update alien positions
        for row in self.alien_matrix:
            for alien in row:
                if self.direction == 'RIGHT':
                    alien.center_x += ALIEN_STEP
                else:
                    alien.center_x -= ALIEN_STEP
                alien.center_y -= self.step_down
                max_x = max(max_x,alien.center_x)
                min_x = min(min_x,alien.center_x)
        
        self.step_down = 0
      
        #If one of the aliens has passed the border, change direction 
        if self.direction == 'RIGHT' and max_x > WIDTH - 50:
            self.direction = 'LEFT'
            self.step_down = ALIEN_STEP
        if self.direction == 'LEFT' and min_x < 50:
            self.direction = 'RIGHT'
            self.step_down = ALIEN_STEP

    def shoot_bullet(self):
        pass
            