import arcade
from config import SCALING

class PlayerBullet(arcade.Sprite):
    def __init__(self, center_x):
        super().__init__('assets/player_bullet.png')
        self.center_x = center_x
        self.center_y = 50
