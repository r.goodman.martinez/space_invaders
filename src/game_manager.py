import arcade
from config import ALIEN_ANIMATION_TIMING, WIDTH, HEIGHT, SCALING, PLAYER_STEP, TITLE
import math
import player
from aliens import Aliens
from player_bullet import PlayerBullet

class Game(arcade.Window):
    def init(self):
        super.__init__(self, WIDTH, HEIGHT, TITLE)
        self.alien_list = None
        self.player = None
        self.player_bullet = None
        self.aliens = None
        self.game_state = None

    def setup(self):
        self.player = player.Player()
        self.aliens = Aliens()
        self.timer = 0
        self.welcome_screen = 1
        self.score = 0
        self.player_fired_bullet = 0
        self.game_state = 'WELCOME'
        
        #Set screen update rate
        self.set_update_rate(1/150)

        self.setup_welcome_screen()
        
    def setup_welcome_screen(self):
        #Set main logo
        self.logo = arcade.Sprite('assets/logo.png', SCALING * 1.5)
        self.logo.center_x = WIDTH / 2
        self.logo.center_y = HEIGHT - 150

        #set "press start" text
        self.press_start_text = f'PRESS SPACE TO START'
    
    def setup_game_over_screen(self):
        self.game_over_text = f'GAME OVER'
        self.restart_text = f'PRESS SPACE TO RESTART'

    def setup_pause_screen(self):
        self.pause_text = f'GAME PAUSED. PRESS ANY KEY TO CONTINUE'

    def draw_welcome_screen(self):
        self.logo.draw()
        arcade.draw_text(self.press_start_text
                            ,WIDTH / 2 - 130
                            ,HEIGHT / 2
                            ,arcade.color.WHITE_SMOKE
                            ,20
                            ,550
                        )

    def draw_game_over_screen(self):
        self.player.kill()
        for row in self.aliens.alien_matrix:
            for alien in row:
                alien.kill()
        arcade.draw_text(self.game_over_text
                            ,WIDTH / 2 - 130
                            ,HEIGHT / 2
                            ,arcade.color.WHITE_SMOKE
                            ,20
                            ,550
                        )
        arcade.draw_text(self.restart_text
                            ,WIDTH / 2 - 130
                            ,HEIGHT / 2 - 100
                            ,arcade.color.WHITE_SMOKE
                            ,20
                            ,550
                        )
    
    def draw_pause_screen(self):
        arcade.draw_text(self.pause_text
                            ,WIDTH / 2 - 130
                            ,HEIGHT / 2
                            ,arcade.color.WHITE_SMOKE
                            ,20
                            ,550
                        )

    def player_fire_bullet(self):
        #Create a bullet at the same x position as the player
        self.player_bullet = PlayerBullet(self.player.center_x)
        self.player_fired_bullet = 1

    def on_draw(self):
        arcade.start_render()

        #Draw the welcome screen or main game
        if self.game_state == 'WELCOME':
            self.draw_welcome_screen()
        elif self.game_state == 'GAME OVER':
            self.draw_game_over_screen()
        elif self.game_state == 'PAUSE':
            self.draw_pause_screen()
        else:   #running
            self.player.draw()
            if self.player_fired_bullet:
                self.player_bullet.draw()
            for row in self.aliens.alien_matrix:
                row.draw()

    def on_key_press(self, key, modifiers):
        if self.game_state == 'WELCOME':
            if key == arcade.key.SPACE:
                for row in self.aliens.alien_matrix:
                    row.update()
                    row.update_animation()
                self.game_state = 'RUNNING'
        elif self.game_state == 'PAUSE':
            self.game_state = 'RUNNING' #any key press will resume the game
        elif self.game_state == 'GAME OVER':
            self.setup()
            self.game_state = 'WELCOME' #any key press will take us back to the main menu
        else:
            if key == arcade.key.RIGHT:
                self.player.change_x = PLAYER_STEP
            if key == arcade.key.LEFT:
                self.player.change_x = PLAYER_STEP * -1
            if key == arcade.key.UP and not self.player_fired_bullet:
                self.player_fire_bullet()
            if key == arcade.key.ESCAPE:
                self.game_state = 'PAUSE'
                self.setup_pause_screen()
                self.draw_pause_screen()

    def on_key_release(self, key, modifiers):
        if key == arcade.key.LEFT or key == arcade.key.RIGHT:
            self.player.change_x = 0

    def check_collisions(self):
        #Check if any of the aliens hits the player or a bullet
        for row in self.aliens.alien_matrix:
            if arcade.check_for_collision_with_list(self.player,row):
                print("GAME OVER")
                self.game_state = 'GAME OVER'
                self.setup_game_over_screen()
                self.draw_game_over_screen()
            for alien in row:
                if self.player_fired_bullet and arcade.check_for_collision(alien,self.player_bullet):
                    self.player_bullet.kill()
                    alien.kill()
                    self.player_fired_bullet = 0

        #Check if there's a player bullet and if it has left the screen
        if self.player_fired_bullet:
            if self.player_bullet.center_y > HEIGHT:
                self.player_fired_bullet = 0
                self.player_bullet.kill()

    def on_update(self, delta_time):
        #TODO: Move each update logic to individual functions
        fps = delta_time * 3600
        print(fps)

        self.timer += 1
        
        if self.game_state == 'RUNNING':
            self.player.update()
            if self.player_fired_bullet:
                self.player_bullet.change_y = 5
                self.player_bullet.update()
            for row in self.aliens.alien_matrix:
                row.update()
                row.update_animation()
            if self.timer/2 >= fps:
                self.timer = 0
                self.aliens.move_aliens()
            self.check_collisions()
            

